# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin:/bin:/sbin:/usr/sbin:/usr/bin:/usr/local/bin:/usr/local/sbin
SVN_EDITOR=/usr/bin/vim
EDITOR=/usr/bin/vim
HISTFILESIZE=1000000000
HISTSIZE=1000000
HISTTIMEFORMAT="%F-%T%t"
shopt -s histappend
export PROMPT_COMMAND='history -a'

gpg-agent --daemon --enable-ssh-support --write-env-file "${HOME}/.gpg-agent-info"

  if [ -f "${HOME}/.gpg-agent-info" ]; then
           . "${HOME}/.gpg-agent-info"
           export GPG_AGENT_INFO
           export SSH_AUTH_SOCK
           export SSH_AGENT_PID
   fi

export PATH EDITOR SVN_EDITOR HISTSIZE HISTFILESIZE
unset USERNAME
export LANG=C
echo 'ALERT - $USER Shell Access on' {$HOSTNAME}: `date` `who` | mail -s "Alert: $USER Access on $HOSTNAME from `who | awk '{print $6}'`" changeme@localhost
